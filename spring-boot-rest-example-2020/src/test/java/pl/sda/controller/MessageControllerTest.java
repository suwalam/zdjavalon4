package pl.sda.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import pl.sda.model.Message;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
@RunWith(SpringRunner.class)
public class MessageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void messageListTest() throws Exception {

        mockMvc.perform(post("/api/messages")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(new Message(1, "hello"))))
                .andExpect(status().isCreated());

        //Najpierw nie wysyłać nic postem i zobaczyć, że get zwróci pustą listę czyli []

        String expectedStr = "[{\"id\":1,\"text\":\"hello\"}]";

        mockMvc.perform(MockMvcRequestBuilders.get("/api/messages")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(objectMapper.writeValueAsString(new Message(1, "hello")))));
    }

    @Test
    public void updateMessageTest() throws Exception {

        mockMvc.perform(post("/api/messages")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(new Message(1, "hello"))))
                .andExpect(status().isCreated());

        mockMvc.perform(MockMvcRequestBuilders.put("/api/messages/{id}", 0)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(new Message(1, "new-hello"))))
                .andExpect(status().isNoContent());

        String expectedStr = "{\"id\":1,\"text\":\"new-hello\"}";

        mockMvc.perform(MockMvcRequestBuilders.get("/api/messages/{id}", 0)
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(objectMapper.writeValueAsString(new Message(1, "new-hello")))));
    }

}