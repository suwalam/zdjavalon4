package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.sda.model.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Body POST
 * {
 * "id": "1",
 * "text": "hello-1"
 * }
 */

@Slf4j
@RestController //Połączenie adnotacji Controller oraz ResponseBody
//@Controller
public class MessageController {

    private final List<Message> messages = new ArrayList<>();


    @GetMapping(path = "/api/messages")
    public List<Message> messages() {
        log.info("Get all messages");
        return messages;
    }

    @ResponseStatus(HttpStatus.CREATED) //Dodać po pokazaniu podstawowych właściwości
    //@ResponseBody //Mapuje obiekt odpowiedzi na ResponseEntity
    //Zamiast pisać method można uzyc adnotacji PostMapping
    @RequestMapping(value = "/api/messages", method = RequestMethod.POST)
    public void createMessage(@RequestBody final Message message) {
        messages.add(message);
        log.info("Added message: " + message.getText() + " as " + messages.size());
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    //@ResponseBody
    @RequestMapping(value = "/api/messages/{index}", method = RequestMethod.PUT)
    public void updateMessage(@RequestBody final Message message, @PathVariable final Integer index) {
        messages.get(index).setText(message.getText());
        log.info("Updated message: " + message.getText() + " with index " + index);
    }

    @ResponseStatus(HttpStatus.OK)
    //@ResponseBody
    @RequestMapping(method = RequestMethod.GET, path = "/api/messages/{index}")
    public Message getMessage(@PathVariable final Integer index) {
        log.info("Get message: " + messages.get(index) + " with index " + index);
        return messages.get(index);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping(path = "/api/messages/{index}")
    public void deleteMessage(@PathVariable final Integer index) {
        log.info("Delete message: " + messages.get(index) + " with index " + index);

        messages.remove(messages.get(index));
        log.info("Size after delete: " + messages.size());
    }
}
