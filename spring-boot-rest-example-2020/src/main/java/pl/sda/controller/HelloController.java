package pl.sda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.sda.model.Message;

@Controller
public class HelloController {

    @RequestMapping(method = RequestMethod.GET, path = "/api/hello")
    public ResponseEntity<Message> sayHello() {
        return ResponseEntity.ok().body(new Message(1, "Hello World"));
    }

    @RequestMapping(method = RequestMethod.GET, path = "/api/hi")
    public ResponseEntity<String> sayHi() {
        return ResponseEntity.ok().body("hi");
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, path = "/api/hello2")
    public Message sayHello2() {
        return new Message(1, "Hello World");
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, path = "/api/hi2")
    public String sayHi2() {
        return "hi";
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, path = "/api/nothing")
    public void doNothing() {
    }

    /**
     *
     * Wykorzystując adnotację @PathVariable pamiętajmy, że:
     *
     * typ zmiennej możemy dopasować do potrzeb (możemy wykorzystać np. Integer, Long, String czy UUID - konwersja nastąpi automatycznie)
     * pole name jest opcjonalne w przypadku gdy nazwa użyta w ścieżce jest równa nazwie argumentu metody (tzn. w poprzednim przykładzie moglibyśmy pole to usunąć, ponieważ zarówno argument metody, jak i zmienna część ścieżki mają nazwę id)
     */

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, path = "/api/hello3/{id}")
    public Message sayHelloWithId(@PathVariable(name = "id") final Integer id) {
        return new Message(id, "Hello World " + id);
    }

}
