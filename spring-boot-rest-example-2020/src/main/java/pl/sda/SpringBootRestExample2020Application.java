package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestExample2020Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestExample2020Application.class, args);
	}

}
