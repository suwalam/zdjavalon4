package pl.sda.mockito.database;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DataServiceImplTest {

    private static final String DATA_VALUE = "VALUE";

    private static final Data DATA = Data.builder().value(DATA_VALUE).build();

    @Mock
    private DatabaseConnection databaseConnection;

    @Spy //używamy wtedy gdy chcemy traktować obiekt rzeczywisty jako atrapa i chcemy śledzić jego wywołania
    private DataRepository dataRepository = new DataRepositoryImpl();

    @InjectMocks
    private DataServiceImpl dataService;

    @Test
    public void shouldAddDataForOpenedDatabaseConnection() {

        //given
        when(databaseConnection.isOpened()).thenReturn(true);

        //when
        Data actualData = dataService.add(DATA);

        //then
        Assertions.assertThat(actualData.getId()).isNotNull();
        Assertions.assertThat(actualData.getValue()).isEqualTo(DATA_VALUE);

        verify(databaseConnection, times(2)).isOpened();
        verify(databaseConnection, never()).open();
        verify(databaseConnection).close();
        verifyNoMoreInteractions(databaseConnection);

        //Ta weryfikacja jest możliwa dzięki użyciu adnotacji @Spy
        verify(dataRepository).add(DATA);
        verifyNoMoreInteractions(dataRepository);
    }

    @Test
    public void shouldAddDataForSecondOpenedDatabaseConnection() {

        //given
        when(databaseConnection.isOpened()).thenReturn(false, true);

        //when
        Data actualData = dataService.add(DATA);

        //then
        Assertions.assertThat(actualData.getId()).isNotNull();
        Assertions.assertThat(actualData.getValue()).isEqualTo(DATA_VALUE);

        verify(databaseConnection, times(3)).isOpened();
        verify(databaseConnection).open();
        verify(databaseConnection).close();
        verifyNoMoreInteractions(databaseConnection);

        //Ta weryfikacja jest możliwa dzięki użyciu adnotacji @Spy
        verify(dataRepository).add(DATA);
        verifyNoMoreInteractions(dataRepository);
    }

    //TODO
    //Praca domowa
    //Zaimplementować ścieżkę negatywną dla przypadku dataService.add


}