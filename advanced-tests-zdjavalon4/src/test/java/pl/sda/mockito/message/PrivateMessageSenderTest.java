package pl.sda.mockito.message;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PrivateMessageSenderTest {

    private static final String TEXT = "hello";

    private static final String AUTHOR_ID = "andrzej";

    private static final String RECIPIENT_ID = "anna";

    @Mock
    private MessageProvider messageProvider;

    @Mock
    private MessageValidator messageValidator;

    @InjectMocks
    private PrivateMessageSender messageSender;

    @Captor
    private ArgumentCaptor<Message> messageCaptor;

    @Test
    public void shouldSendPrivateMessage() {

        //given
        when(messageValidator.isMessageValid(any())).thenReturn(true);
        when(messageValidator.isMessageRecipientReachable(RECIPIENT_ID)).thenReturn(true);

        doNothing().when(messageProvider).send(any(), eq(MessageType.PRIVATE));

        //when
        messageSender.sendPrivateMessage(TEXT, AUTHOR_ID, RECIPIENT_ID);

        //then
        verify(messageValidator).isMessageValid(any());
        verify(messageValidator).isMessageRecipientReachable(RECIPIENT_ID);
        verifyNoMoreInteractions(messageValidator);
        verify(messageProvider).send(any(), any());
        verifyNoMoreInteractions(messageProvider);
    }

    @Test
    public void shouldSendPrivateMessageWithCaptorUse() {

        //given
        when(messageValidator.isMessageValid(any())).thenReturn(true);
        when(messageValidator.isMessageRecipientReachable(RECIPIENT_ID)).thenReturn(true);

        doNothing().when(messageProvider).send(any(), eq(MessageType.PRIVATE));

        //when
        messageSender.sendPrivateMessage(TEXT, AUTHOR_ID, RECIPIENT_ID);

        //then
        verify(messageProvider).send(messageCaptor.capture(), eq(MessageType.PRIVATE));
        verify(messageValidator).isMessageValid(messageCaptor.capture());

        verify(messageValidator).isMessageRecipientReachable(RECIPIENT_ID);
        verifyNoMoreInteractions(messageValidator);
        verifyNoMoreInteractions(messageProvider);

        List<Message> messageCaptorAllValues = messageCaptor.getAllValues();

        org.junit.jupiter.api.Assertions.assertEquals(2, messageCaptorAllValues.size());

        for(Message message : messageCaptorAllValues) {
            assertMessage(message);
        }
    }

    private void assertMessage(Message messageCaptorValue) {
        Assertions.assertThat(messageCaptorValue.getAuthor()).isEqualTo(AUTHOR_ID);
        Assertions.assertThat(messageCaptorValue.getRecipient()).isEqualTo(RECIPIENT_ID);
        Assertions.assertThat(messageCaptorValue.getValue()).isEqualTo(TEXT);
        Assertions.assertThat(messageCaptorValue.getSendAt()).isNotNull();
        Assertions.assertThat(messageCaptorValue.getId()).isNotNull();
    }

    //TODO
    //Praca domowa
    //Zaimplementować ścieżkę negatywną dla przypadku messageSender.sendPrivateMessage

}


