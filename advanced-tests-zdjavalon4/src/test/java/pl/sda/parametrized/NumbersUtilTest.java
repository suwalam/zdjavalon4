package pl.sda.parametrized;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class NumbersUtilTest {

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 67, 761, -5})
    public void shouldReturnTrueForOddNumbers(int number) {
        assertTrue(NumbersUtil.isOdd(number));
    }

    @ParameterizedTest
    @ValueSource(ints = {2, 6, 88, 760, -20})
    public void shouldReturnFalseForEvenNumbers(int number) {
        assertFalse(NumbersUtil.isOdd(number));
    }

    @ParameterizedTest
    @MethodSource(value = "provideNumbersWithInfoAboutParity")
    public void shouldReturnExpectedValueForGivenNumber(int number, boolean expected) {
        assertEquals(expected, NumbersUtil.isOdd(number));
    }

    @ParameterizedTest
    @ArgumentsSource(NumberWithParityArgumentsProvider.class)
    public void shouldReturnExpectedValueForGivenNumberWithArgumentsSource(int number, boolean expected) {
        assertEquals(expected, NumbersUtil.isOdd(number));
    }

    private static Stream<Arguments> provideNumbersWithInfoAboutParity() {
        return Stream.of(
                Arguments.of(1, true),
                Arguments.of(11, true),
                Arguments.of(0, false),
                Arguments.of(10, false),
                Arguments.of(761, true)
        );
    }

    @Test
    public void shouldThrowIllegalArgumentException() {
        //Metody assertThrows oraz assertEquals pochodzą z JUnit
        IllegalArgumentException e =
                assertThrows(IllegalArgumentException.class, () -> NumbersUtil.divide(10, 0));

        assertEquals("dividend can't be 0", e.getMessage());
    }

    @Test
    public void shouldThrowIllegalArgumentException_v2() {
        //Metody pochodzą z AssertJ

        Assertions
                .assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> NumbersUtil.divide(11, 0))
                .withMessage("dividend can't be 0");
    }

    //TODO
    //Praca domowa
    //Należy zaimplemtować test dla przypadku pozytywnego NumbersUtil.divide





}