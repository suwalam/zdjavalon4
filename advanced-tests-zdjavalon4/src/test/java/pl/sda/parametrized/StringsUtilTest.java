package pl.sda.parametrized;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import static org.junit.jupiter.api.Assertions.*;

class StringsUtilTest {

    @ParameterizedTest
    //given
    @CsvSource(value = {"   test  ;TEST", "teST;TEST", "  Java;JAVA"}, delimiter = ';')
    public void shouldTrimAndUpperCaseInput(String input, String expected) {
        //when
        String actual = StringsUtil.toUpperCase(input);

        //then
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    //given
    @CsvFileSource(resources = "/data.csv", numLinesToSkip = 1, delimiter = ',', lineSeparator = ";",
            encoding = "UTF-8")
    public void shouldTrimAndUpperCaseInputCSVFile(String input, String expected) {
        //when
        String actual = StringsUtil.toUpperCase(input);

        //then
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @NullSource
    public void shouldBeBlankForNull(String input) {
        //when //then
        assertTrue(StringsUtil.isBlank(input));
    }

    @ParameterizedTest
    @EmptySource
    public void shouldBeBlankForEmptyString(String input) {
        //when //then
        assertTrue(StringsUtil.isBlank(input));
    }

    @ParameterizedTest
    @NullAndEmptySource
    public void shouldBeBlankForNullAndEmptyString(String input) {
        //when //then
        assertTrue(StringsUtil.isBlank(input));
    }

}