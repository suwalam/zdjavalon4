package pl.sda.parametrized;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ArraysUtilTest {

    @ParameterizedTest
    @NullAndEmptySource
    public void shouldReturnFalseForNullAndEmptyList(List<String> list) {
        assertFalse(ArraysUtil.isValid(list));
    }

    @ParameterizedTest
    @MethodSource("provideListAndExpectedValue")
    public void shouldReturnExpectedValueForGivenList(List<String> list, boolean expected) {
        assertEquals(expected, ArraysUtil.isValid(list));
    }

    private static Stream<Arguments> provideListAndExpectedValue() {
       return Stream.of(
                Arguments.of(Arrays.asList("ala", "ma", "kota"), true),
                Arguments.of(new ArrayList<String>(), false),
                Arguments.of(null, false)
        );
    }

}