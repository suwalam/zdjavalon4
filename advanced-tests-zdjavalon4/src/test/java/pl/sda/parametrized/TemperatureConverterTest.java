package pl.sda.parametrized;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.*;

class TemperatureConverterTest {

    @ParameterizedTest
    @EnumSource(TemperatureConverter.class)
    public void shouldConvertTempToValueMinThan273_15(TemperatureConverter temperatureConverter) {
        assertTrue(temperatureConverter.convertTemp(0) >= -273.15F);
    }

    @ParameterizedTest
    @EnumSource(value = TemperatureConverter.class, names = {"^.*CELSIUS*."}, mode = EnumSource.Mode.MATCH_ALL)
    public void shouldConvertTempToValueMinThan273_15_v2(TemperatureConverter temperatureConverter) {
        assertTrue(temperatureConverter.convertTemp(0) >= -273.15F);
    }
}