package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBookCatalogJavawwa31Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBookCatalogJavawwa31Application.class, args);
	}

}
