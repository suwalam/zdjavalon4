package pl.sda.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.model.Role;
import pl.sda.model.User;
import pl.sda.service.AutoLoginService;
import pl.sda.service.RoleService;
import pl.sda.service.UserService;

import java.util.Arrays;

@Slf4j
@Controller
public class RegistrationController {

    private final UserService userService;

    private final RoleService roleService;

    private final AutoLoginService autoLoginService;

    public RegistrationController(final UserService userService, final RoleService roleService, final AutoLoginService autoLoginService) {
        this.userService = userService;
        this.roleService = roleService;
        this.autoLoginService = autoLoginService;
    }

    @GetMapping("/registration")
    public ModelAndView registration() {
        ModelAndView modelAndView = new ModelAndView("registration");
        modelAndView.addObject("user", new User());

        return modelAndView;
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("user") User user) {

        if (userService.findByUsername(user.getUsername()) != null) {
            log.info("USER exists! " + user.getUsername());
            return "registration";
        }

       Role role = roleService.findByName("USER");

        user.setRoles(Arrays.asList(role));

        userService.save(user);

        log.info("Registered user: " + user.getUsername());

        autoLoginService.autoLogin(user.getUsername(), user.getPassword());

        return "redirect:/book-list";

    }

}
